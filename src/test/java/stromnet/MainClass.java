package stromnet;

import java.util.Scanner;

public class MainClass {
    private static final int EXIT_ACTION = 1;
    private static final int CREATE_ARRAY_ACTION = 2;
    private static final int FILL_ARRAY_ACTION = 3;
    private static final int SORT_ARRAY_ASC_ACTION = 4;
    private static final int SORT_ARRAY_DESC_ACTION = 5;
    private static final int PRINT_ARRAY_ASC_ACTION = 6;

    public static void main(String[] args) {
        int[] array = null;
        Scanner in = new Scanner(System.in);
        ArrayOperator arrayOperator = new ArrayOperator();
        System.out.println("Enter menu item: ");
        int item = in.nextInt();

        while(item != EXIT_ACTION){
            switch (item) {
                case CREATE_ARRAY_ACTION:
                    System.out.println("Enter array length, please: ");
                    int length = in.nextInt();
                    array = arrayOperator.createArray(length);
                    arrayOperator.printArray(array);
                    break;
                case FILL_ARRAY_ACTION:
                    if(array != null){
                        arrayOperator.fillArray(array);
                        arrayOperator.printArray(array);
                    }else {
                        System.out.println("Error! Please, create(2) and fill(3) array first...");
                    }
                    break;
                case SORT_ARRAY_ASC_ACTION:
                    if(array != null){
                        arrayOperator.sortArrayByAsc(array);
                        System.out.println("Sorted by ASC array: ");
                        arrayOperator.printArray(array);
                    }else {
                        System.out.println("Error! Please, create(2) and fill(3) array first...");
                    }
                    break;
                case SORT_ARRAY_DESC_ACTION:
                    if(array != null){
                        arrayOperator.sortArrayByDesc(array);
                        System.out.println("Sorted by DESC array: ");
                        arrayOperator.printArray(array);

                    }else {
                        System.out.println("Error! Please, create(2) and fill(3) array first...");
                    }
                    break;
                case PRINT_ARRAY_ASC_ACTION:
                    if(array != null){
                        arrayOperator.printArray(array);
                    }else {
                        System.out.println("Error! Please, create(2) and fill(3) array first...");
                    }
                    break;
                default:
                    System.out.println("Error! Incorrect menu item, try again...");
            }

            System.out.println("\nEnter menu item or '1' to exit: ");
            item = in.nextInt();
        }

        System.exit(0);
    }
}
