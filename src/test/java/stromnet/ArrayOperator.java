package stromnet;

import java.util.Random;

public class ArrayOperator {
    private int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("Max must be greater than min");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    private boolean isSortedByAsc(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                return false;
            }
        }
        return true;
    }

    private boolean isSortedByDesc(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] < array[i + 1]) {
                return false;
            }
        }
        return true;
    }

    public int[] createArray(int length){
        return new int[length];
    }

    public void fillArray(int[] array){
        for(int i = 0; i < array.length; i++){
            array[i] = getRandomNumberInRange(1, 25);
        }
    }

    public void sortArrayByAsc(int[] array){
        while (!isSortedByAsc(array)) {
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    int tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                }
            }
        }
    }

    public void sortArrayByDesc(int[] array){
        while (!isSortedByDesc(array)) {
            for (int i = 0; i < array.length-1; i++) {
                if (array[i] < array[i + 1]) {
                    int tmp = array[i + 1];
                    array[i + 1] = array[i];
                    array[i] = tmp;
                }
            }
        }
    }

    public void printArray(int[] array){
        for (int i = 0; i < array.length; i++){
            System.out.print(array[i] + ", ");
        }
    }

}
